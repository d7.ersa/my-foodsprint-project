import {ModuleWithProviders, NgModule} from '@angular/core';
import {PreloadAllModules, Router, RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./home/home.component";
import {ErrorPageComponentComponent} from "./error-page-component/error-page-component.component";
import {CommonModule} from "@angular/common";


const routes: Routes = [
  { path: '', redirectTo:'/home', pathMatch:'full'},
  { path: 'home', component: HomeComponent},
  { path: '**', component: ErrorPageComponentComponent}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
