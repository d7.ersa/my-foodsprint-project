import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  registerForm! : FormGroup;


  constructor(public fb: FormBuilder, public auth: AuthService) {

  }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      displayName: ["", [Validators.required]],
      email: ["", [Validators.required]],
      password: ["", [Validators.required]],
      address: ["", [Validators.required]],
      role: ["user", [Validators.required]]
    });
  }

  setData(){
    let user: any = {
      email: this.registerForm.get('email')?.value,
      displayName: this.registerForm.get('displayName')?.value,
      address: this.registerForm.get('address')?.value,
      role: this.registerForm.get('role')?.value,
      password: this.registerForm.get('password')?.value,
    };
    this.auth.signUp(user);
  }

  onSignUp() {
    this.setData();
  }
}
