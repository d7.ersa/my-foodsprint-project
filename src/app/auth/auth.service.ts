import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import firebase from "firebase";
import UserF = firebase.User;
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import { AngularFireAuth } from "@angular/fire/auth";
import {User} from "../_models/user";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userData: any | undefined;   //logged in user info

  constructor(
    public afs: AngularFirestore,   // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router
  ) {
      this.afAuth.authState.subscribe(user => {
        if (user) { ///If user logged in save his info in local storage
          this.userData = user;
          localStorage.setItem('user', JSON.stringify(this.userData));
          JSON.parse(<string>localStorage.getItem('user'));
        } else { ///else setting his value in null
          localStorage.setItem('user', "{ }");
          JSON.parse(<string>localStorage.getItem('user'));
        }
      })
    }

  get isLoggedIn(): boolean {
    const user = JSON.parse(<string>localStorage.getItem('user'));
    return (user !== null);
  }

  signUp(user: any) {
    return this.afAuth.createUserWithEmailAndPassword(user.email, user.password)
      .then((result) => {
        this.setUserData(result.user, user);
      }).catch((error) => {
        console.log("This error comes from signUp: "+error)
      })
  }


  private setUserData(user: any, formData: any) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: formData.email,
      displayName: formData.displayName,
      address: formData.address,
      role: formData.role
    }
    return userRef.set(userData, {
      merge: true
    })
  }

  signIn(email: string, password: string) {
    return this.afAuth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${result.user?.uid}`);
        userRef.get().subscribe(res=> {
          this.userData = res.data();
          if(this.userData.role == 'user'){
              this.router.navigate(['dashboard']);
          }
          else if(this.userData.role == 'restaurant') {
            this.router.navigate(['restaurant/menu']);
          }
        });
      }).catch((error) => {
        console.log("Error from sign in:" + error);
      })
  }

  signOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['login']);
    })
  }
}
