import { Component, OnInit } from '@angular/core';
import { AuthService } from "../auth.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  templateForm!: FormGroup;

  constructor(private fb: FormBuilder, public authService: AuthService) {
  }

  ngOnInit(): void {
    this.templateForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    let userLogin =
    {
      email: this.templateForm.get('email')?.value,
      password: this.templateForm.get('password')?.value
    }
    ;
    this.authService.signIn(userLogin.email, userLogin.password);
  }
}

