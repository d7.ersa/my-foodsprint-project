import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {RestaurantComponent} from "./restaurant/restaurant.component";
import {UserComponent} from "./user/user.component";
import {MenuComponent} from "./restaurant/menu/menu.component";
import {DashboardComponent} from "./user/dashboard/dashboard.component";




const routes: Routes = [
  {
    path: 'restaurant',
    component: RestaurantComponent,
    children: [
      {
        path: 'menu',
        component: MenuComponent,
      },
    ],
  },
  {
    path: 'user',
    component: UserComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
    ],
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [
    RouterModule
  ]

})
export class UsersRoutingModule { }
