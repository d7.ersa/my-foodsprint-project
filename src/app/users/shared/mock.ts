import {Dish} from "../../_models/dish";
import {Category} from "../../_models/category";

export class Mock{
  static DISHES: Dish[] = [
    {
      did: '1',
      name: 'Pasta with mushrooms',
      description: 'pasta, soy sauce, mushrooms, salt',
      category: new Category('Pasta'),
      price: 2500
    },
    {
      did: '2',
      name: 'Pasta with sauce',
      description: 'pasta, soy sauce, salt',
      category: new Category('Pasta'),
      price: 2500
    },
    {
      did: '3',
      name: 'Baked feta pasta',
      description: 'pasta, soy sauce, mushrooms, salt',
      category: new Category('Pasta'),
      price: 2500
    },
    {
      did: '4',
      name: 'Salad',
      description: 'tomatoes, salad',
      category: new Category('Salad'),
      price: 2500
    }
  ]


}
