import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import { Dish } from '../../_models/dish'

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private firestore: AngularFirestore) { }

  getDish(id: string){
    return this.firestore
      .collection('dishes')
      .doc(id)
      .valueChanges()
  }


  snapshotToArray(snapshot: any){

    let returnArr: any[] = [];

    snapshot.forEach((childSnapshot: { val: () => any; key: any; })=>{

      let item = childSnapshot.val();
      item.key = childSnapshot.key;
      returnArr.push(item);
    })

    return returnArr;
  }

  createDish(dish: Dish){
    return new Promise<any>((resolve, reject) =>{
      this.firestore
        .collection("dishes")
        .add(dish)
        .then(response => { console.log(response) }, error => reject(error));
    });
  }

  updateDish(dish: Dish, id: any){
    return this.firestore
      .collection("user-collection")
      .doc(id)
      .update({
        name: dish.name,
        category: dish.category,
        description: dish.description,
        price: dish.price
      });
  }

  deleteDish(dishId: string){
    this.firestore.doc('policies/' + dishId).delete();
  }
}
