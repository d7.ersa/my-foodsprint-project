import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-add-dish',
  templateUrl: './add-dish.component.html',
  styleUrls: ['./add-dish.component.css']
})
export class AddDishComponent implements OnInit {

  closeResult = '';
  constructor(private modalAddDish: NgbModal) { }

  ngOnInit(): void {
  }

  open(content: any){
    this.modalAddDish.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): any {
    if (reason === ModalDismissReasons.ESC) {
      console.log(reason + 'by pressing ESC')
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      console.log(reason + 'by clicking on a backdrop')
    } else {
      return `with: ${reason}`;
    }
  }


  //funksion per te kapur kategorite missing
}
