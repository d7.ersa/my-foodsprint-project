import { Component, OnInit } from '@angular/core';
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {CrudService} from "../../shared/crud.service";
import {Dish} from "../../../_models/dish";

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {

  closeResult: any;
  dish: Dish | undefined;

  constructor(private modalAddCat: NgbModal, public crud: CrudService) { }

  open(content: any){
    this.modalAddCat.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): any {
    if (reason === ModalDismissReasons.ESC) {
      console.log(reason + 'by pressing ESC')
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      console.log(reason + 'by clicking on a backdrop')
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit(): void {
  }

}
