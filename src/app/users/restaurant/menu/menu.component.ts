import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {DecimalPipe} from "@angular/common";
import {Observable} from "rxjs";
import {NgbdSortableHeader, SortEvent} from "../../shared/sortable.directive";
import {DishService} from "../../shared/dish.service";
import {Dish} from "../../../_models/dish";
import {CrudService} from "../../shared/crud.service";
import firebase from "firebase";
import Firestore = firebase.firestore.Firestore;

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  providers: [DishService, DecimalPipe]})

export class MenuComponent implements OnInit {
  dishes$: Observable<Dish[]>;
  total$: Observable<number>;

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader> | undefined;

  constructor(public service: DishService) {

    this.dishes$ = service.dishes$;
    this.total$ = service.total$;
  }


  onSort({column, direction}: SortEvent) {
      // resetting other headers
      this.headers?.forEach(header => {
        if (header.sortable !== column) {
          header.direction = '';
        }
      });

      this.service.sortColumn = column;
      this.service.sortDirection = direction;
  }



  ngOnInit(): void {
    /*
    this.firestore.collection('dishes')
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach(
          (doc: { data: () => any; }) => this.service.DISHES.push(doc.data()));
      })
      */
  }
}
