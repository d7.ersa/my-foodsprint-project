import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubnavRestaurantComponent } from './subnav-restaurant.component';

describe('SubnavRestaurantComponent', () => {
  let component: SubnavRestaurantComponent;
  let fixture: ComponentFixture<SubnavRestaurantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubnavRestaurantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubnavRestaurantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
