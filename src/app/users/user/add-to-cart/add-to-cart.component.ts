import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-to-cart',
  templateUrl: './add-to-cart.component.html',
  styleUrls: ['./add-to-cart.component.css']
})
export class AddToCartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


  increaseCount($event: MouseEvent, el: HTMLInputElement) {
    let value = parseInt(el.value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    el.value = String(value);
  }

  decreaseCount($event: MouseEvent, el: HTMLInputElement) {
    let value = parseInt(el.value, 10);
    if (value > 1) {
      value = isNaN(value) ? 0 : value;
      value--;
      el.value = String(value);
    }
  }
}
