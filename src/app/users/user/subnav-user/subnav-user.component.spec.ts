import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubnavUserComponent } from './subnav-user.component';

describe('SubnavUserComponent', () => {
  let component: SubnavUserComponent;
  let fixture: ComponentFixture<SubnavUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubnavUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubnavUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
