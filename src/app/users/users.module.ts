import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuComponent } from './restaurant/menu/menu.component';

import { SubnavUserComponent } from './user/subnav-user/subnav-user.component';
import { DashboardComponent } from './user/dashboard/dashboard.component';
import {SubnavRestaurantComponent} from "./restaurant/subnav-restaurant/subnav-restaurant.component";
import { CategoryComponent } from './shared/category/category.component';
import { DishComponent } from './shared/dish/dish.component';
import { AddToCartComponent } from './user/add-to-cart/add-to-cart.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbdSortableHeader } from "./shared/sortable.directive";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from "@angular/platform-browser";
import { AddDishComponent } from './restaurant/add-dish/add-dish.component';
import { AddCategoryComponent } from './restaurant/add-category/add-category.component';
import { UsersRoutingModule } from "./users-routing.module";


@NgModule({
  declarations: [
    DashboardComponent,
    CategoryComponent,
    SubnavRestaurantComponent,
    SubnavUserComponent,
    MenuComponent,
    DishComponent,
    AddToCartComponent,
    NgbdSortableHeader,
    AddDishComponent,
    AddCategoryComponent

  ],
  exports: [
    SubnavRestaurantComponent,
    SubnavUserComponent,
    MenuComponent,
    AddCategoryComponent,
    UsersRoutingModule
  ],
  imports: [
      CommonModule,
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      NgbModule,
      UsersRoutingModule
  ],
})
export class UsersModule { }
