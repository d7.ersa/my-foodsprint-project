
import {User} from "./user";
import {Dish} from "./dish";

export interface Order{
  oid: string;
  pid: Dish;
  userId: User;
  restaurantId: User;
  quantity: number;
  status: boolean;
}
