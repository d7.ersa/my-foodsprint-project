export class Category {
  constructor(pasta: string) {
    this.name = pasta;
  }

  name: string | undefined;
}
