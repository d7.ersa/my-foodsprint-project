import { Category } from "./category";

export interface Dish{
  did: string;
  name: string;
  category: Category;
  description: string;
  price: number;
}
