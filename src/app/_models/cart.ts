import {User} from "./user";
import {Order} from "./order";

export interface Cart{
  userId: User;
  orderId: Order;
  status: boolean;
}
