// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC24QY-hlNw-JRrxaM6GJhXiypev_8U628",
    authDomain: "foodsprint-1-2bd7f.firebaseapp.com",
    projectId: "foodsprint-1-2bd7f",
    storageBucket: "foodsprint-1-2bd7f.appspot.com",
    messagingSenderId: "478185116529",
    appId: "1:478185116529:web:d7a6a726118367e2a35cd0",
    measurementId: "G-PFMY1WH0CH"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
